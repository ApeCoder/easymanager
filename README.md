# jardock

#### 介绍
jar集群部署管理工具

使用[Solon](https://gitee.com/noear/solon) 开发，利用socketd的反向远程调用功能实现jar文件的多机部署

### 部署
项目分为server和agent两部分，server包含管理界面和服务，agent不含管理界面。可以只部署server。
部署是直接启动使用java启动对应的jar即可。

```
java -jar server.jar --server.port=996
```
agent启动时需要指定server端的地址,如:

```
java -jar agent.jar --manager.address=192.168.0.110:996
```

agent启动后自动连接到server端，可以通过server端控制agent进行jar的部署(新建服务，上传jar文件，启动停止，查看日志)

默认的用户名密码为：admin/admin
![登录](https://images.gitee.com/uploads/images/2021/1206/164034_f2c00903_364416.png "9177F3B7-9FCD-4CB6-A02A-3067A7C18D90.png")
![服务管理](https://images.gitee.com/uploads/images/2021/1206/164142_e53309e9_364416.png "57CB47E8-EEB0-483C-9154-0A36E349C707.png")
![服务详情](https://images.gitee.com/uploads/images/2021/1206/164242_53ab8ca8_364416.png "88BDFB49-F184-40DC-BC84-F96579EE5E09.png")
![文件管理](https://images.gitee.com/uploads/images/2021/1206/164337_919de1bd_364416.png "84A0F487-EF97-4DFA-B461-13E81EFF893B.png")
![Ngin管理(仅server端)](https://images.gitee.com/uploads/images/2021/1206/164427_8a67e597_364416.png "E605E655-4A0D-4EF2-AD60-84F687259919.png")
