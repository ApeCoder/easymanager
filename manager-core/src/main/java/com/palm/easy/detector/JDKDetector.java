package com.palm.easy.detector;

import org.noear.solon.extend.health.detector.AbstractDetector;

import java.util.HashMap;
import java.util.Map;

public class JDKDetector extends AbstractDetector {
    @Override
    public String getName() {
        return "jdk";
    }

    @Override
    public Map<String, Object> getInfo() {
        Map<String,Object> info=new HashMap<>();
        info.put("version",System.getProperty("java.version"));
        info.put("vendor",System.getProperty("java.vendor"));
        info.put("user_dir",System.getProperty("user.dir"));
        info.put("java_home",System.getProperty("java.home"));
        return info;
    }
}
