package com.palm.easy.service;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.palm.easy.ManagerConfiguration;
import com.palm.easy.domain.Service;
import com.palm.easy.exception.BusinessException;
import com.palm.easy.util.StringUtil;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jutils.jprocesses.JProcesses;

import org.noear.solon.Utils;
import org.noear.solon.core.util.PrintUtil;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author desire
 */
@Data
@AllArgsConstructor
public class ServerContext {
    private String servicePath;
    private HashMap<String, Service> serviceMap;
   // private Map<String, Process> processMap = new ConcurrentHashMap<>();

    public String host() {
        return ManagerConfiguration.localIp();
    }
    public Service getService(String name){
        Service service=serviceMap.get(name);
        if(service==null){
            throw new BusinessException("未发现服务 "+name);
        }
        return service;
    }
//    public Process findProcess(String serviceName){
//        return processMap.get(serviceName);
//    }
    public boolean stopService(String serviceName) {
        Service sv=serviceMap.get(serviceName);
//        Process process = processMap.get(serviceName);
//        if (process != null) {
//            process.destroyForcibly();
//            sv.setRunning(false);
//            processMap.remove(serviceName);
//            sv.setProcessId(null);
//        }else{
        if(sv.isRunning()&&sv.getProcessId()!=null){

            JProcesses.killProcess(sv.getProcessId().intValue());
            sv.setRunning(false);
            sv.setProcessId(null);
        }
//        }
        saveServiceToFile(sv);
        return true;

    }
    public void deleteService(Service service) {
        this.stopService(service.getName());
        serviceMap.remove(service.getName());
        File dir = new File(servicePath, service.getName());
        Util.deleteDir(dir);
        // db.commit();
    }
    public void saveService(Service service) {

        Service old = getService(service.getName());
        old.setArguments(service.getArguments());
        old.setAuto(service.isAuto());
        old.setMapping(service.isMapping());
        old.setCommands(service.getCommands());
        old.setHome(service.getHome());
        old.setContextPath(service.getContextPath());
        old.setPort(service.getPort());
        old.setTitle(service.getTitle());
        old.setVmOptions(service.getVmOptions());
        old.setMemo(service.getMemo());
        old.setServiceName(service.getServiceName());
        old.setEnabled(service.isEnabled());
        old.setFound(service.isFound());
        old.setMappingPath(service.getMappingPath());
        old.setMappingSetting(service.getMappingSetting());
        old.setArguments(service.getArguments());
        old.setUrl(service.getUrl());
        old.setJavaPath(service.getJavaPath());

        serviceMap.put(service.getName(), old);
        saveServiceToFile(service);
        //  db.commit();
    }

    public File getJar(Service service) {
        File dist = new File(servicePath, service.getName() + "/"+service.getName()+".jar");
        return dist;
    }

    public void updateFile(Service service, Map<String, String> configs, byte[] file) {
        service = getService(service.getName());
        if (!"jar".equals(service.getType())) {
            throw new RuntimeException("服务类型不正确");
        }
        File dist = getJar(service);
        if (!dist.getParentFile().exists()) {
            dist.getParentFile().mkdirs();
        }
        boolean reStart = false;
//        if (processMap.containsKey(service.getName())) {
//            reStart = true;
//            this.stopService(service.getName());
//        }
        this.stopService(service.getName());
        try (OutputStream out = new FileOutputStream(dist)) {
            out.write(file);
            out.flush();
            out.close();
            if (reStart) {
                configs.put("port", service.getPort() + "");
                this.startJar(service, configs);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public boolean startCommand(Service service, Map<String, String> configs) {
        File workDir = new File(service.getWorkDir());
        if (!workDir.exists()) {
            throw new RuntimeException("工作目录错误");
        }
        if (StringUtil.isEmpty(service.getCommands())) {
            throw new RuntimeException("命令不能为空");
        }
        String cmds = processConfig(service.getCommands(), configs);
        List<String> commands = Util.splitPrams(cmds);

        //
        StringBuilder commandStr=new StringBuilder("nohup ");
        commandStr.append(StringUtil.join(commands," "));
        commandStr.append(" >> logs/info-`date +%Y-%m-%d`.log 2>&1 & \n echo $!>.pid");

        StringUtil.write(new File(workDir,"start.sh"),commandStr.toString());

        ProcessBuilder chmod = new ProcessBuilder("chmod","+x","./start.sh");
        chmod.directory(workDir);
        try{
            chmod.start().waitFor();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //mkdir
        new File(workDir,"logs").mkdirs();
        //
        File pidFile = new File(workDir,".pid");
        if(pidFile.exists()){
            pidFile.delete();
        }

        ProcessBuilder pb = new ProcessBuilder(commands);
        pb.directory(workDir);
        Utils.pools.submit(() -> {
            try {
                final Process process = pb.start();
                service.setLastStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                //targetService.setProcessId(Util.getPid(process));
                service.setRunning(true);
               // processMap.put(service.getName(), process);
                String pid=StringUtil.readFrom(new File(workDir,".pid")).trim();
                service.setProcessId(Integer.valueOf(pid));
//                service.setProcessId(Util.getPid(process));
                saveServiceToFile(service);
                PrintUtil.info("[START] " + String.join(" ", commands));

//                Logger logger = getLogger(service);
//
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//                String line;
//                try {
//                    while ((line = bufferedReader.readLine()) != null) {
//                        logger.info(line);
//                    }
//                } catch (IOException io) {
//
//                }
//                process.waitFor();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return true;
    }

    public boolean startJar(Service targetService, Map<String, String> configs) {
        File jar = getJar(targetService);
        File workDir=jar.getParentFile();
        if (!jar.exists()) {
            throw new RuntimeException("没有找到对应的jar");
        }
        String javaPath = targetService.getJavaPath();
        if(StringUtil.isEmpty(javaPath)) {
            javaPath = System.getProperty("java.home") + "/bin/java";
        }
        List<String> commands = new ArrayList<>();
        commands.add(javaPath);
        String jvmOpts = targetService.getVmOptions();
        if (!Utils.isEmpty(jvmOpts)) {
            jvmOpts = processConfig(jvmOpts, configs);
            commands.addAll(Util.splitPrams(jvmOpts));
        }
        commands.add("-jar");
        commands.add(jar.getName());
        commands.add("--config=application.properties");

        String args = targetService.getArguments();
        if (!Utils.isEmpty(args)) {
            args = processConfig(args, configs);
            commands.addAll(Util.splitPrams(args));
        }

        //write application.properties
        StringBuilder properties=new StringBuilder();
        properties.append("server.port=");
        properties.append(targetService.getPort());
        String manager=configs.get("manager");
        if(StringUtil.isNotEmpty(manager)){
            properties.append("\neasy.manager=" + manager);
        }
        String token=configs.get("token");
        if(StringUtil.isNotEmpty(token)){
            properties.append("\neasy.token=" + token);
        }
        StringUtil.write(new File(workDir,"application.properties"),properties.toString());
        //
        StringBuilder commandStr=new StringBuilder("nohup ");
        commandStr.append(StringUtil.join(commands," "));
        commandStr.append(" >> logs/info-`date +%Y-%m-%d`.log 2>&1 & \necho $!>.pid");

        StringUtil.write(new File(workDir,"start.sh"),commandStr.toString());

        ProcessBuilder chmod = new ProcessBuilder("chmod","+x","./start.sh");
        chmod.directory(workDir);
        try{
            chmod.start().waitFor();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //mkdir
        new File(workDir,"logs").mkdirs();
        //
        File pidFile = new File(workDir,".pid");
        if(pidFile.exists()){
            pidFile.delete();
        }

        ProcessBuilder pb = new ProcessBuilder("./start.sh");
        pb.directory(workDir);
        // executorService.submit(() -> {

        try {
            Process process = pb.start();
            process.waitFor();
            //targetService.setProcessId(Util.getPid(process));
            String pidStr=StringUtil.readFrom(new File(workDir,".pid")).trim();
            int pid= Integer.valueOf(pidStr);

           if(Util.findProcess(pid)){
               targetService.setLastStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
               targetService.setRunning(true);

               targetService.setProcessId(pid);
               saveServiceToFile(targetService);
               PrintUtil.info("[START] " + String.join(" ", commands));
           }else{
               targetService.setRunning(false);
               targetService.setProcessId(null);
               saveServiceToFile(targetService);
               PrintUtil.info("[START FAIL] " + String.join(" ", commands));
               return false;
           }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
    private String processConfig(String command, Map<String, String> configs) {
        if (configs == null || configs.size() == 0) {
            return command;
        }
        for (Map.Entry<String, String> e : configs.entrySet()) {
            command = command.replaceAll("\\$\\{\\s+" + e.getKey() + "\\s+\\}", e.getValue());
        }
        return command;
    }
    public void saveServiceToFile(Service service) {
        String content = JSON.toJSONString(service, JSONWriter.Feature.PrettyFormat);// ONode.stringify(service);
        File dir = new File(servicePath, service.getName());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        StringUtil.write(new File(dir, "service.json"), content);

    }
}
