package com.palm.easy.service.impl;

import com.alibaba.fastjson2.JSON;
import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import com.palm.easy.service.IServer;
import com.palm.easy.util.StringUtil;
import com.palm.easy.util.Util;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Init;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class LocalServer implements IServer {
    HashMap<String, Service> serviceMap;
    Map<String, Process> processMap = new ConcurrentHashMap<>();

    private String servicePath;

    @Init
    public void init() {
        // executorService = Executors.newCachedThreadPool();
        //serviceMap = db.createHashMap("services").makeOrGet();
        String basePath = ManagerConfiguration.localPath();
        servicePath = new File(basePath, "services/").getAbsolutePath();
        serviceMap = new HashMap<>();
        File servicesDir = new File(servicePath);
        if (!servicesDir.exists()) {
            servicesDir.mkdirs();
        }
        for (File sdir : servicesDir.listFiles()) {
            if (sdir.isDirectory()) {
                File s = new File(sdir, "service.json");
                if (s.exists()) {
                    Service service = JSON.parseObject(StringUtil.readFrom(s), Service.class);
                    if (service.isRunning() && service.getProcessId() != null) {
                        boolean isRunning = Util.findProcess(service.getProcessId());
                        service.setRunning(isRunning);
                    } else {
                        service.setRunning(false);
                    }
                    serviceMap.put(service.getName(), service);
                }
            }
        }
    }
    @Override
    public <R> R execute(IServerAction<R> action) {
        return action.execute(new ServerContext(servicePath,serviceMap));
    }
}
