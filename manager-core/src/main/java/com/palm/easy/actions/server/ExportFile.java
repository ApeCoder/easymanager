package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.FileData;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.noear.solon.Utils;

import java.io.*;

@Data
@AllArgsConstructor
public class ExportFile implements IServerAction<FileData> {
    private String path;
    @Override
    public FileData execute(ServerContext context) {
        Util.checkPath(path);
        File f = new File(ManagerConfiguration.localPath(), path);
        if (!f.exists()) {
            throw new RuntimeException("文件不存在");
        }
        if (f.isDirectory()) {//压缩导出
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                Util.toZip(f.getAbsolutePath(), out, true);
                FileData fout = new FileData();
                fout.setName(f.getName() + ".zip");
                fout.setData(out.toByteArray());
                return fout;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try (InputStream in = new FileInputStream(f)) {
                byte[] data = Utils.transferToBytes(in);
                FileData fout = new FileData();
                fout.setName(f.getName());
                fout.setData(data);
                return fout;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
