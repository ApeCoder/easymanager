package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import lombok.Data;

@Data
public class DeleteService implements IServerAction<Void> {
    private Service service;
    public DeleteService(Service service){
        this.service=service;
    }
    @Override
    public Void execute(ServerContext context) {
         context.deleteService(service);
         return null;
    }
}
