package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import com.palm.easy.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
@Data
@AllArgsConstructor
public class StartService implements IServerAction<Boolean> {
    private String serviceName;
    private Map<String, String> configs;
    @Override
    public Boolean execute(ServerContext context) {
//        if (context.getProcessMap().containsKey(serviceName)) {
//            throw new BusinessException("服务已经启动");
//        }
        Service targetService = context.getService(serviceName);
        if(targetService.isRunning()){
            throw new BusinessException("服务已经启动");
        }
        configs.put("port", targetService.getPort() + "");
        if ("command".equals(targetService.getType())) {
            return context.startCommand(targetService, configs);
        } else {
            return context.startJar(targetService, configs);
        }
    }
}
