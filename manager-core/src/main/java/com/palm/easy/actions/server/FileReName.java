package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
@Data
@AllArgsConstructor
public class FileReName implements IServerAction<Boolean> {
    String path;
    String newName;
    @Override
    public Boolean execute(ServerContext context) {
        Util.checkPath(path);
        File file = new File(ManagerConfiguration.localPath(), path);
        if (!file.exists()) {
            return false;
        }
        if(newName.indexOf(File.separator)>-1||newName.indexOf("..")>-1){
            return false;
        }
        return file.renameTo(new File(file.getParentFile(),newName));
    }
}
