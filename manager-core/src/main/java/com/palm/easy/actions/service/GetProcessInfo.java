package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;

@Data
@AllArgsConstructor
public class GetProcessInfo implements IServerAction<ProcessInfo> {
    private String service;
    @Override
    public ProcessInfo execute(ServerContext context) {

        Service serv = context.getService(service);
        Integer pid=serv.getProcessId();
        if(pid!=null&&pid>0){
            return JProcesses.getProcess(pid);
        }
        return null;
    }
}
