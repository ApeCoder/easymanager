package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.util.Util;
import lombok.Data;

import java.io.*;


@Data
public class UploadFile implements IServerAction<Boolean> {
    private String path;
    private String name;
    private byte[] data;


    @Override
    public Boolean execute(ServerContext context) {
        Util.checkPath(path);
        File dir = new File(ManagerConfiguration.localPath(), path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File dist = new File(dir, name);
        try (OutputStream fout = new FileOutputStream(dist)) {
            fout.write(data);
            data=null;
            fout.flush();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return true;
    }
}
