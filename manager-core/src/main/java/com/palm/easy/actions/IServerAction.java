package com.palm.easy.actions;

import com.palm.easy.service.ServerContext;

import java.io.Serializable;

public interface IServerAction<R> extends Serializable {
    R execute(ServerContext context);
}
