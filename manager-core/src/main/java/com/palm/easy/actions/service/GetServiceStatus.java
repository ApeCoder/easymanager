package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.detector.FileDetector;
import com.palm.easy.detector.ProcessDetector;
import com.palm.easy.domain.Service;
import com.palm.easy.util.Util;
import com.sun.management.OperatingSystemMXBean;
import lombok.Data;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;
@Data
public class GetServiceStatus implements IServerAction<Map<String, Object>> {

    private String serviceName;
    @Override
    public Map<String, Object> execute(ServerContext context) {
        Service service=context.getService(serviceName);

        Map<String, Object> info = new HashMap<>();

        Map<String, Object> processInfo = new HashMap<>();
        processInfo.put("CPU", 0);
        processInfo.put("MEM", 0);
        processInfo.put("RSS", 0);//内存使用量

        OperatingSystemMXBean osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        long totalMemory = osmxb.getTotalPhysicalMemorySize();
        processInfo.put("TotalMemory", totalMemory);
        info.put("process", processInfo);


        if ("jar".equals(service.getType())) {
            FileDetector fileDetector = new FileDetector(context.getJar(service));
            info.put("file", fileDetector.getInfo());
        } else {
            FileDetector fileDetector = new FileDetector(new File(service.getWorkDir()));
            info.put("file", fileDetector.getInfo());
        }
        return info;
    }
}
