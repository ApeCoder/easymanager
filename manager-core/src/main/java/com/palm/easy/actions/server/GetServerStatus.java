package com.palm.easy.actions.server;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.detector.FileDetector;
import com.palm.easy.detector.JDKDetector;
import org.noear.solon.extend.health.detector.CpuDetector;
import org.noear.solon.extend.health.detector.Detector;
import org.noear.solon.extend.health.detector.MemoryDetector;
import org.noear.solon.extend.health.detector.OsDetector;

import java.util.HashMap;
import java.util.Map;

public class GetServerStatus implements IServerAction<Map<String,Object>> {
    @Override
    public Map<String, Object> execute(ServerContext context) {

        Detector[] allDetectors = new Detector[]{
                new CpuDetector(),
                new OsDetector(),
                new MemoryDetector(),
                new FileDetector(),
                new JDKDetector()
        };

        Map<String, Object> info = new HashMap<>();
        for (Detector detector : allDetectors) {
            info.put(detector.getName(), detector.getInfo());
        }
        return info;
    }
}
