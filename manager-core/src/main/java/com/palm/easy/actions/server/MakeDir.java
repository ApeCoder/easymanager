package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.exception.BusinessException;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
@Data
@AllArgsConstructor
public class MakeDir implements IServerAction<String> {
    private String path;
    private String file;

    @Override
    public String execute(ServerContext context) {
        Util.checkPath(path);
        File f = new File(ManagerConfiguration.localPath(), path);
        if (!f.exists() && !f.isDirectory()) {
            throw new BusinessException("文件已存在，且不是文件夹:"+path);
        }
        File distFile=new File(f,file);
        if(distFile.exists()){
            throw new BusinessException("文件已存在");
        }
        distFile.mkdirs();
        return file;
    }
}
