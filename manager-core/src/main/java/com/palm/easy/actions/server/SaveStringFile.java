package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.util.StringUtil;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;

@Data
@AllArgsConstructor
public class SaveStringFile implements IServerAction<String> {

    private String path;
    private String file;
    @Override
    public String execute(ServerContext context) {
        Util.checkPath(path);
        File f = new File(ManagerConfiguration.localPath(), path);
        if (f.exists() && f.isDirectory()) {
            throw new RuntimeException("文件路径错误");
        }
        StringUtil.write(f, file);
        return path;
    }
}
