package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.FileInfo;
import com.palm.easy.util.StringUtil;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
public class ListFile implements IServerAction<List<FileInfo>> {
    private String path;
    @Override
    public List<FileInfo> execute(ServerContext context) {
        if (StringUtil.isEmpty(path)) {
            path = "/";
        }
        Util.checkPath(path);
        File file = new File(ManagerConfiguration.localPath(), path);
        if (!file.exists()) {
            return new ArrayList<>();
        }
        List result = new ArrayList();
        for (File f : file.listFiles()) {
            String name = f.getName();
            String type = "unknown";
            if (f.isDirectory()) {
                type = "dir";
            } else if (name.indexOf(".") > -1) {
                type = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
            }

            FileInfo fi = new FileInfo(name, f.length(), f.lastModified(), type);
            result.add(fi);
        }
        return result;
    }
}
