package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StopService implements IServerAction<Boolean> {
    private String serviceName;

    @Override
    public Boolean execute(ServerContext context) {
        return context.stopService(serviceName);
    }
}
