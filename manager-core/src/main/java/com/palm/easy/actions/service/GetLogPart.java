package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.LogPart;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.noear.solon.Solon;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 获取服务日志的一部分，用于打印实时日志
 */
@Data
@AllArgsConstructor
public class GetLogPart implements IServerAction<LogPart> {
    //服务名
    private String service;
    //取日志的偏移
    private long pos;
    @Override
    public LogPart execute(ServerContext context) {
        String basePath = Solon.cfg().get("local.path", "./manager/");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        File logFile = new File(basePath, "services/" + service + "/logs/info-" + date + ".log");
        if (!logFile.exists()) {
            return new LogPart();
        }
        try (RandomAccessFile raf = new RandomAccessFile(logFile, "r")) {
            long length = raf.length();
            if (pos == 0) {
                //如果pos==0，获取最后10行
                int rows = 50;
                byte[] c = new byte[1];
                // 在获取到指定行数和读完文档之前,从文档末尾向前移动指针,遍历文档每一个字节
                for (long pointer = raf.length(), lineSeparatorNum = 0; pointer >= 0 && lineSeparatorNum < rows; ) {
                    // 移动指针
                    raf.seek(pointer--);
                    // 读取数据
                    int readLength = raf.read(c);
                    if (readLength != -1 && c[0] == 10) {
                        lineSeparatorNum++;
                    }
                    // 扫描完依然没有找到足够的行数,将指针归0
                    if (pointer == -1 && lineSeparatorNum < rows) {
                        raf.seek(0);
                    }
                }
            } else if (pos < length) {
                raf.seek(pos);
            } else {
                return new LogPart();
            }

            byte[] tempbytes = new byte[(int) (raf.length() - raf.getFilePointer())];
            raf.readFully(tempbytes);
            String content = new String(tempbytes, "UTF-8");
            return new LogPart(length, content);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new LogPart();
    }
}
