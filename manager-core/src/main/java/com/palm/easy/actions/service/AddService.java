package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;

import java.util.HashMap;

public class AddService implements IServerAction<Void> {
    public Service service;
    public AddService(){

    }
    public AddService(Service service) {
        this.service = service;
    }

    @Override
    public Void execute(ServerContext context) {

        HashMap<String, Service> serviceMap=context.getServiceMap();
        if (serviceMap.containsKey(service.getName())) {
            throw new RuntimeException("已存在同名服务️");
        }
        serviceMap.put(service.getName(), service);
        context.saveServiceToFile(service);
        return null;
    }
}
