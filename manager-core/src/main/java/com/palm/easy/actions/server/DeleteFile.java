package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
@Data
@AllArgsConstructor
public class DeleteFile implements IServerAction<Boolean> {
    String path;
    @Override
    public Boolean execute(ServerContext context) {
        Util.checkPath(path);
        File file = new File(ManagerConfiguration.localPath(), path);
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            return file.delete();
        }
        return Util.deleteDir(file);
    }
}
