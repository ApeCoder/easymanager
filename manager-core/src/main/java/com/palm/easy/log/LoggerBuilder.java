package com.palm.easy.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.rolling.RollingFileAppender;

public class LoggerBuilder {


    public static Logger build(String name,String path) {
        RollingFileAppender infoAppender = new AppenderFactory().createRollingFileAppender(path, Level.INFO);
        LoggerContext context = (LoggerContext) infoAppender.getContext();
        Logger logger = context.getLogger(name);
        //设置不向上级打印信息
        logger.setAdditive(false);
        logger.addAppender(infoAppender);
        return logger;
    }
    public static Logger buildForError(String name,String path) {
        RollingFileAppender infoAppender = new AppenderFactory().createRollingFileAppender(path, Level.ERROR);
        LoggerContext context = (LoggerContext) infoAppender.getContext();
        Logger logger = context.getLogger(name);
        //设置不向上级打印信息
        logger.setAdditive(false);
        logger.addAppender(infoAppender);
        return logger;
    }
}
