package com.palm.easy.domain;

import java.util.ArrayList;
import java.util.List;

public class ServiceFoundItem {
    String name;
    String title;
    List<String> urls=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }
}
