package com.palm.easy.domain;

import java.io.Serializable;

public class FileData implements Serializable {
    String name;
    byte[] data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
