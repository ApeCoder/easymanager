package com.palm.easy.domain;

import lombok.Data;

import java.io.Serializable;
@Data
public class Service implements Serializable {
    //服务唯一ID
    private String name;

    //服务名，网关映射和服务管理用
    private String serviceName;

    //服务别称
    private String title;

    //服务是否启用，禁用的服务不会启动，也不会出现在映射列表，服务发现列表
    private boolean enabled;

    //是否映射到网关
    private boolean mapping;
    //映射路径，规则为 http://server:port/mappingPath/ => /serviceName/
    private String mappingPath;
    private String mappingSetting;
    //是否自动启动
    private boolean auto;


    //所属服务器
    private String server;


    //进程PID
    private Integer processId;

    //对外端口
    private int port;

    //服务是否在运行
    private boolean running;

    private boolean found;
    //contextPath，用于服务发现调用时的路径，规则为： http://server:port/contextPath/path => /serviceName/path
    private String contextPath;

    private String url;
    //首页,指定后可直接跳转，描述性字段
    private String home;
    //备注
    private String memo;

    private String type="jar";//jar command url
//    private String file;
    //command 模式下的workDir,jar模式固定为jar所在目录（service.name命名）
    private String workDir;
    //command 模式下命令
    private String commands;
    //jar 模式下使用
    private String arguments;
    //jar 模式下使用
    private String vmOptions;

    private String lastStartTime;
    //java 路径，若是为空，则使用manager的路径
    private String javaPath;

}
