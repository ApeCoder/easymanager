package com.palm.easy.util;


import com.palm.easy.exception.BusinessException;
import lombok.SneakyThrows;
import org.jutils.jprocesses.JProcesses;

import java.io.*;
import java.lang.reflect.Field;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.*;

public class Util {
    private static final int BUFFER_SIZE = 2 * 1024;
    private static String localAddress;

    /**
     * 获取本地地址
     */
    public static String getLocalAddress() {
        if (localAddress == null) {
            InetAddress address = findFirstNonLoopbackAddress();
            localAddress = address.getHostAddress();
        }

        return localAddress;
    }

    private static InetAddress findFirstNonLoopbackAddress() {
        InetAddress result = null;
        try {
            int lowest = Integer.MAX_VALUE;
            for (Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces(); nics
                    .hasMoreElements(); ) {
                NetworkInterface ifc = nics.nextElement();
                if (ifc.isUp()) {
                    if (ifc.getIndex() < lowest || result == null) {
                        lowest = ifc.getIndex();
                    } else if (result != null) {
                        continue;
                    }
                    for (Enumeration<InetAddress> addrs = ifc.getInetAddresses(); addrs.hasMoreElements(); ) {
                        InetAddress address = addrs.nextElement();
                        if (address instanceof Inet4Address && !address.isLoopbackAddress()) {
                            result = address;
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println("Cannot get first non-loopback address" + ex.getStackTrace());
        }

        if (result != null) {
            return result;
        }

        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.err.println("Unable to retrieve localhost");
        }
        return null;
    }

    public static List<String> splitPrams(String args) {
        List<String> r = new ArrayList<>();
        int start = 0;
        int flag = 0;
        char fc = 0;
        char bf = 0;
        for (int i = 0; i < args.length(); i++) {
            char c = args.charAt(i);
            if ((flag == 0 && c == ' ' && bf != '\\')) {
                String s = args.substring(start, i);
                if (s.trim().length() > 0) {
                    r.add(s);
                }
                start = i + 1;
            }
            if (i == args.length() - 1) {
                String s = args.substring(start, i + 1);
                if (s.trim().length() > 0) {
                    r.add(s);
                }
                break;
            }
            bf = c;
            if (c == '\'' || c == '"') {
                if (fc == 0) {
                    fc = c;
                    flag++;
                } else if (fc == c) {
                    flag--;
                }
            }

        }

        return r;
    }

    /**
     * 压缩成ZIP 方法1
     *
     * @param srcDir           压缩文件夹路径
     * @param out              压缩文件输出流
     * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
     *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws RuntimeException 压缩失败会抛出运行时异常
     */
    public static void toZip(String srcDir, OutputStream out, boolean KeepDirStructure)
            throws RuntimeException {

        long start = System.currentTimeMillis();
        ZipOutputStream zos = null;
        try {
            zos = new ZipOutputStream(out);
            File sourceFile = new File(srcDir);
            compress(sourceFile, zos, sourceFile.getName(), KeepDirStructure);
            long end = System.currentTimeMillis();
            System.out.println("压缩完成，耗时：" + (end - start) + " ms");
        } catch (Exception e) {
            throw new RuntimeException("zip error from ZipUtils", e);
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 递归压缩方法
     *
     * @param sourceFile       源文件
     * @param zos              zip输出流
     * @param name             压缩后的名称
     * @param KeepDirStructure 是否保留原来的目录结构,true:保留目录结构;
     *                         false:所有文件跑到压缩包根目录下(注意：不保留目录结构可能会出现同名文件,会压缩失败)
     * @throws Exception
     */
    private static void compress(File sourceFile, ZipOutputStream zos, String name, boolean KeepDirStructure) throws Exception {
        byte[] buf = new byte[BUFFER_SIZE];
        if (sourceFile.isFile()) {
            // 向zip输出流中添加一个zip实体，构造器中name为zip实体的文件的名字
            zos.putNextEntry(new ZipEntry(name));
            // copy文件到zip输出流中
            int len;
            FileInputStream in = new FileInputStream(sourceFile);
            while ((len = in.read(buf)) != -1) {
                zos.write(buf, 0, len);
            }
            // Complete the entry
            zos.closeEntry();
            in.close();
        } else {
            File[] listFiles = sourceFile.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                // 需要保留原来的文件结构时,需要对空文件夹进行处理
                if (KeepDirStructure) {
                    // 空文件夹的处理
                    zos.putNextEntry(new ZipEntry(name + "/"));
                    // 没有文件，不需要文件的copy
                    zos.closeEntry();
                }
            } else {
                for (File file : listFiles) {
                    // 判断是否需要保留原来的文件结构
                    if (KeepDirStructure) {
                        // 注意：file.getName()前面需要带上父文件夹的名字加一斜杠,
                        // 不然最后压缩包中就不能保留原来的文件结构,即：所有文件都跑到压缩包根目录下了
                        compress(file, zos, name + "/" + file.getName(), KeepDirStructure);
                    } else {
                        compress(file, zos, file.getName(), KeepDirStructure);
                    }
                }
            }
        }
    }

    /**
     * 解压文件到指定目录
     * 解压后的文件名，和之前一致
     *
     * @param zipFile 待解压的zip文件
     * @param descDir 指定目录
     */
    @SuppressWarnings("rawtypes")
    public static void unZipFiles(File zipFile, String descDir) throws IOException {
        Charset charset = System.getProperty("os.name").toLowerCase().indexOf("windows") > -1 ? Charset.forName("GBK") : Charset.defaultCharset();
        ZipFile zip = new ZipFile(zipFile, charset);//解决中文文件夹乱码
        String name = zip.getName().substring(zip.getName().lastIndexOf('\\') + 1, zip.getName().lastIndexOf('.'));
        File pathFile = new File(descDir + name);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }

        for (Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            String zipEntryName = entry.getName();
            InputStream in = zip.getInputStream(entry);
            String outPath = (descDir + name + "/" + zipEntryName).replaceAll("\\*", "/");

            // 判断路径是否存在,不存在则创建文件路径
            File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
            if (!file.exists()) {
                file.mkdirs();
            }
            // 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
            if (new File(outPath).isDirectory()) {
                continue;
            }
            // 输出文件路径信息
//          System.out.println(outPath);

            FileOutputStream out = new FileOutputStream(outPath);
            byte[] buf1 = new byte[1024];
            int len;
            while ((len = in.read(buf1)) > 0) {
                out.write(buf1, 0, len);
            }
            in.close();
            out.close();
        }
        return;
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    protected final static String osName = System.getProperty("os.name").toLowerCase();
    protected final static boolean isWindows = osName.indexOf("windows") > -1;

    public static long getPid(Process process) {
        return process.pid();
//        if (isWindows) {
//            //windows
//            return new WinProcess(process).getPid();
//        } else {
//            try {
//                Field field = process.getClass().getDeclaredField("pid");
//                field.setAccessible(true);
//                int handle = (int) field.get(process);
//                return handle;
//            } catch (NoSuchFieldException | IllegalAccessException e) {
//                e.printStackTrace();
//            }
//        }
//        return 0;
    }

    public static List<String> execute(String... command) {
        String text = null;
        List<String> result = new ArrayList<>();
        try {
            ProcessBuilder builder = new ProcessBuilder(new String[0]);
            builder.command(command);
            Process process = builder.start();
            process.getOutputStream().close();
            try (InputStream is = process.getInputStream()) {
                InputStreamReader isr = new InputStreamReader(is);
                LineNumberReader lnr = new LineNumberReader(isr);
                while ((text = lnr.readLine()) != null) {
                    result.add(text);
                }
                lnr.close();
                isr.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean findProcess(int pid) {
       // return JProcesses.getProcess((int)pid)!=null;
        if (isWindows) {
            return execute("tasklist", "/FI", "\"PID eq " + pid + "\"").size() > 3;
        } else {
            return execute("ps", "-p", pid + "").size() > 1;
        }
    }

    public static void stopProcess(long pid) {
        if (isWindows) {
            execute("taskkill", "-PID", pid + "", "-F");
        } else {
            execute("kill", "-9", pid + "");
        }
    }

    public static void checkPath(String path) {
        if (path == null || path.indexOf("..") > -1) {
            throw new BusinessException("路径错误:"+path);
        }
    }

    @SneakyThrows
    public byte[] gzip(byte[] data) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (GZIPOutputStream gout = new GZIPOutputStream(out)) {
            gout.write(data);
        }
        return out.toByteArray();
    }

    @SneakyThrows
    public byte[] unGzip(byte[] data) {
        ByteArrayInputStream bi = new ByteArrayInputStream(data);
        try (GZIPInputStream gin = new GZIPInputStream(bi)) {
            return gin.readAllBytes();
        }
    }
}
