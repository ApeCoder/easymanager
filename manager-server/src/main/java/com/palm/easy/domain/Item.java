package com.palm.easy.domain;


import java.io.Serializable;
import java.util.List;

public class Item implements Serializable,Comparable<Item> {
    private String key;
    private String title;
    private String parent;
    private Integer order;
    private List<Item> children;
    public Item(){

    }
    public Item(String key, String title) {
        this.key = key;
        this.title = title;
    }

    @Override
    public int compareTo(Item o) {
        int thisOrder=order==null?0:this.order;
        int thatOrder=o.order==null?0:o.order;
        return this.order-thatOrder;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<Item> getChildren() {
        return children;
    }

    public void setChildren(List<Item> children) {
        this.children = children;
    }
}
