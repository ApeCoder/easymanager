package com.palm.easy.anno;


import com.palm.easy.anno.impl.AuthHandler;
import org.noear.solon.annotation.Before;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限控制
 */
@Before(AuthHandler.class)
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Auth {
    //需要登录
    boolean login() default true;
}
