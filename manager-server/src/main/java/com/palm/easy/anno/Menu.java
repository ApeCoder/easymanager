package com.palm.easy.anno;

import java.lang.annotation.*;

/**
 * 声明菜单
 */
@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Menu {
    //title
    String value();
    //String id() default "";
    String path() default "";
    String icon() default "";
    String parent() default "";
    int order() default 0;
}
