package com.palm.easy;

import com.palm.easy.anno.Menu;

public interface SystemMenus {
    @Menu(value="服务管理",path = "services.vue",icon="icon-server",order = -2)
    String Home="Home";
    @Menu(value="服务器监控",path = "server.vue",icon="icon-air-play",order=-1)
    String Server="Server";
    @Menu(value="服务器控制台",path = "sshClient.vue",icon="  icon-monitor")
    String SSH="SSH";
    @Menu(value="文件管理",path = "files.vue",icon="icon-folder",order = 1)
    String File="File";
    @Menu(value="配置管理",path = "configs.vue",icon="icon-paper",order = 2)
    String Config="Config";
    @Menu(value="Nginx管理",path = "nginx.vue",icon="icon-globe",order = 3)
    String Nginx="Nginx";
//    @Menu(value="任务调度",path = "nginx.vue",icon="h-icon-task",order = 4)
//    String Task="Task";
}
