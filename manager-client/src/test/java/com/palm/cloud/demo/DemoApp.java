package com.palm.cloud.demo;

import com.palm.cloud.detector.Detector;
import org.noear.solon.Solon;

public class DemoApp {
    public static void main(String[] args) {
        Solon.start(DemoApp.class,args,app->{
            Detector.init(app);
            new Thread(()->{
                while (true){
                    System.out.println(Detector.get());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

            }).start();
            fb(50);
            System.out.println("==============================");
        });
    }
    public static int fb(int i){
        if(i<=1){
            return 1;
        }
        return fb(i-1)+fb(i-2);
    }
}
